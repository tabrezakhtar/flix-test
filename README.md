# Code test for Flix Premiere
#### Completed by Tabrez Akhtar

To run the app:

`npm install`

`npm start`

The app will start on port 3000.

Open `localhost:3000`

Run unit tests:  
`npm test`

## Description

Loading films from flix premiere api