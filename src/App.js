import React, {useState, useEffect} from 'react';
import './App.css';
import FilmsList from './films';

function App() {
  const [films, setFilms] = useState([]);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await fetch('https://api.flixpremiere.com/v1/films/filter/now_showing?limit=10');
        const {films} = await result.json()
        setFilms(films);
      } catch(error) {
        setError(error.message);
      }
    };

    fetchData();
    
    setInterval(() => {
      fetchData();
    }, 5000);
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <ul>
          <FilmsList minDuration={5500} films={films} error={error}/>
        </ul>
      </header>
    </div>
  );
}

export default App;
