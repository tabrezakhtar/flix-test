import React from 'react';

function FilmsList({minDuration, films, error}) {

  if(error) {
    return <li>{error}</li>
  }

  return films.length ? films.map(film => 
    film.duration_seconds > minDuration ? <li key={film.id}>{film.title} ({film.duration_seconds})</li> : null
  ) : null;
}

export default FilmsList;