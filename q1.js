function evenValues(values) {
  return values.reduce( (acc, current) => current % 2 === 0 ? acc + current : acc, 0)
}

const nums = [2, 3, 4, null, 6, 'a', 3];
const nums2 = [2, 3, 4];

console.log(evenValues(nums))